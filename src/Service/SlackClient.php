<?php


namespace App\Service;


use App\Helper\LoggerTrait;
use Symfony\Component\Notifier\ChatterInterface;
use Symfony\Component\Notifier\Message\ChatMessage;

class SlackClient
{
    use LoggerTrait;

    /**
     * @var ChatterInterface
     */
    private $chatter;

    public function __construct(ChatterInterface $chatter)
    {
        $this->chatter = $chatter;
    }

    public function sendMessage($message)
    {
        $this->logInfo('Beaming the message to slack!');

        $slackMessage = (new ChatMessage($message))
            ->transport('slack');

        $this->chatter->send($slackMessage);
    }
}