<?php

namespace App\DataFixtures;

use App\Factory\ApiTokenFactory;
use App\Factory\ArticleFactory;
use App\Factory\CommentFactory;
use App\Factory\TagFactory;
use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        UserFactory::new()->many(5)->create();

        UserFactory::new()->many(1)->create(function () {
            return ['roles' => ['ROLE_ADMIN']];
        });

        ApiTokenFactory::new()->many(20)->create(function () {
            return ['user' => UserFactory::random()];
        });

        TagFactory::new()->many(20)->create();

        ArticleFactory::new()
            ->many(10)
            ->create(function () {
                return [
                    'author' => UserFactory::random(),
                    'tags' => TagFactory::randomSet(3)
                ];
            });

        ArticleFactory::new()
            ->unpublished()
            ->many(10)
            ->create(function () {
                return [
                    'author' => UserFactory::random(),
                    'tags' => TagFactory::randomSet(3)
                ];
            });

        CommentFactory::new()->many(50)->create(function () {
            return ['article' => ArticleFactory::random()];
        });
    }
}
